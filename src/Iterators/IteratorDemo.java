package Iterators;

import java.util.Iterator;
import java.util.LinkedList;

public class IteratorDemo {
	public static void main(String[] args) {
		LinkedList<String> list = new LinkedList<>();

		list.add("balakrishna");
		list.add("chiru");
		list.add("venky");
		list.add("nag");

		//System.out.println(list);
		
		Iterator<String> iterator=list.iterator();
		//iterator.remove(); IllegalStateException
		
		System.out.println("Looping througn Iterator");
		while(iterator.hasNext()) {
			System.out.println(iterator.next());
		}
		
		System.out.println("Heros Looping through streams");
		
		System.out.println(list);
		list.stream().forEach(hero->System.out.println(hero));
		
		System.out.println("Heros Looping through enhanced forLoop");
		for(String s:list) {
			System.out.println(s);
		}
		
	}
}
