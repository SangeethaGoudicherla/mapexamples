package Iterators;

import java.util.LinkedList;
import java.util.ListIterator;

public class ListIteratorDemo {
	public static void main(String[] args) {
		
		LinkedList<String> l = new LinkedList<>();
		
		l.add("balakrishna");
		l.add("chiru");
		l.add("venky");
		l.add("nag");
		
		System.out.println(l);
		
		ListIterator<String> ltr = l.listIterator();


		while (ltr.hasNext()) {
			String s = (String) ltr.next();
			if (s.equals("nag")) {
				ltr.add("chaitanya");
			}
		}
		System.out.println(l);
	}
}
