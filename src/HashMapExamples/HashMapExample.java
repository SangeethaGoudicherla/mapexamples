package HashMapExamples;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class HashMapExample {
	static Map<String, Integer> hashMap = new HashMap<>();

	public static Map<String, Integer> createHashMap() {

		hashMap.put("A", 1);
		hashMap.put("B", 2);
		hashMap.put("C", 3);
		hashMap.put(null, null);
		hashMap.put(null, 4);
		hashMap.put("D", null);
		hashMap.put("E", null);

		return hashMap;

	}//A,B,C,D,E,null

	public static void main(String[] args) {
		
		Map<String, Integer> hashMap = HashMapExample.createHashMap();

		System.out.println("Is HashMap empty:::" + hashMap.isEmpty());

		System.out.println("HashMap size::" + hashMap.size());

		hashMap.keySet().forEach(key -> System.out.println("Only keys::" + key));

		hashMap.values().forEach(value -> System.out.println("Only values::" + value));

		Set<Map.Entry<String, Integer>> entrySet = hashMap.entrySet();
		System.out.println(entrySet);

		hashMap.keySet().forEach(s -> {
			System.out.println("Key::" + s);
			System.out.println("Value::" + hashMap.get(s));
		});

		if (hashMap.containsKey("A")) {
			System.out.println("Get value of key : A: " + hashMap.get("A"));
		}

		hashMap.remove("A", 1);

		Set<Map.Entry<String, Integer>> entrySet1 = hashMap.entrySet();
		System.out.println(entrySet1);
	}
}
