package HashMapExamples;

import java.util.ArrayList;
import java.util.List;

public class Student {

	public Student(Long studentId, String studentName) {
		super();
		this.studentId = studentId;
		this.studentName = studentName;
	}

	private Long studentId;
	private String studentName;
	List<Course> courses = new ArrayList<>();

	public List<Course> getCourses() {
		return courses;
	}

	public void setCourses(List<Course> courses) {
		this.courses = courses;
	}

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

}
