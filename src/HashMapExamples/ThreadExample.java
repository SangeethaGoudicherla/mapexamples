package HashMapExamples;

import java.util.Map;

public class ThreadExample extends Thread {
	public void run() {

		Map<String, Integer> hashMap = HashMapExample.createHashMap();
		System.out.println("Thread name : "+Thread.currentThread().getName());
		System.out.println(hashMap.keySet());
	}

	public static void main(String[] args) {
		System.out.println();
		ThreadExample thread1 = new ThreadExample();
		thread1.setName("Thread1");
		thread1.start();
		
		ThreadExample thread2 = new ThreadExample();
		thread2.setName("Thread2");
		thread2.start();
		
		ThreadExample thread3 = new ThreadExample();
		thread3.setName("Thread3");
		thread3.start();
	}
}
