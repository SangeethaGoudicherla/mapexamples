package HashMapExamples;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class SynchronizedDemo extends Thread {
	static Map<String, Integer> hashMap = new HashMap<>();
	static Map<String, Integer> synchronizedMap = null;

	public void run() {
		System.out.println("Thread name : " + Thread.currentThread().getName());
		System.out.println(synchronizedMap.keySet());
	}

	public static void main(String[] args) {

		hashMap.put("A", 1);
		hashMap.put("B", 2);
		hashMap.put("C", 3);
		hashMap.put("D", 4);
		hashMap.put("E", 4);
		hashMap.put("F", 4);
		hashMap.put("G", 4);

		synchronizedMap = Collections.synchronizedMap(hashMap);
		SynchronizedDemo thread1 = new SynchronizedDemo();
		thread1.setName("Thread1");
		thread1.start();

		SynchronizedDemo thread2 = new SynchronizedDemo();
		thread2.setName("Thread2");
		thread2.start();

		SynchronizedDemo thread3 = new SynchronizedDemo();
		thread3.setName("Thread3");
		thread3.start();
	}
}
