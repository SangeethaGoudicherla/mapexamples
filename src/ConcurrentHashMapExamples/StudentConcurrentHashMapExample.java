package ConcurrentHashMapExamples;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import HashMapExamples.Course;
import HashMapExamples.Student;

public class StudentConcurrentHashMapExample {
	public static void main(String[] args) {
		
		Map<Student, List<Course>> studentCourseMap = new ConcurrentHashMap<>();
		List<Course> courseList1 = new ArrayList<>();
		List<Course> courseList2 = new ArrayList<>();

		Course course1 = new Course(101l, "Java");
		Course course2 = new Course(102l, "SQL");
		courseList1.add(course1);
		courseList1.add(course2);

		Course course3 = new Course(103l, "Spring boot and microservices");
		Course course4 = new Course(104l, "Hibernate");
		courseList2.add(course3);
		courseList2.add(course4);

		Student student1 = new Student(1l, "a");
		Student student2 = new Student(2l, "b");
		studentCourseMap.put(student1, courseList1);
		studentCourseMap.put(student2, courseList2);

		studentCourseMap.keySet().forEach(student -> {
			System.out.println("Student Name ::   " + student.getStudentName());
			System.out.println("Courses:::::    ");
			studentCourseMap.get(student).forEach(course -> System.out.println(course.getCourseName()));
			
		});

	}
}
